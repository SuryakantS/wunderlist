//
//  FleetDetailModel.swift
//  WunderFleet
//
//  Created by Suryakant Sharma on 19/08/19.
//  Copyright © 2019 Suryakant Sharma. All rights reserved.
//

import Foundation

struct FleetDetailModel : Decodable {
    let carId: Int?
    let title : String?
    let vehicleTypeId : Int?
    let licencePlate : String?
    let vehicleTypeImageUrl: String?
    let isDamaged : Bool?
    let damageDescription: String?
    let pricingTime: String?
    let lat: Double?
    let lon: Double?
}
