//
//  FleetModel.swift
//  WunderFleet
//
//  Created by Suryakant Sharma on 17/08/19.
//  Copyright © 2019 Suryakant Sharma. All rights reserved.
//

import Foundation

struct FleetModel: Decodable {
    let carId: Int?
    let title: String?
    let lat: Double?
    let lon: Double?
    let licencePlate : String?
}
