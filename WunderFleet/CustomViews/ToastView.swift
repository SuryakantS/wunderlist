//
//  Toast.swift
//  WunderFleet
//
//  Created by Suryakant Sharma on 15/08/19.
//  Copyright © 2019 Suryakant Sharma. All rights reserved.
//

import UIKit

private let AnimationDuration = 2.0
private let BottomPadding:CGFloat = 70
private let Tag = 1100
private let leftRightPadding: CGFloat = 20
private let textPadding: CGFloat = 8

class ToastView: UIView {
    fileprivate let label: UILabel
    var duration = AnimationDuration
    init(message: String , trucateMiddle : Bool = false,numberOfLines: Int = 1) {
        self.label = UILabel()
        self.label.textColor = UIColor.white
        self.label.text = message
        self.label.textAlignment = NSTextAlignment.center
        self.label.font = UIFont.systemFont(ofSize: 15)
        self.label.numberOfLines = (!trucateMiddle) ? 0 : numberOfLines;
        if(trucateMiddle){
            self.label.lineBreakMode = NSLineBreakMode.byTruncatingMiddle;
        }
        super.init(frame: CGRect.zero)
        
        self.alpha = 0
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 8
        self.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        
        self.addSubview(self.label)
        
        guard let delegate = UIApplication.shared.delegate else { return }
        if let window = delegate.window {
            // Removing the already presented toast view.
            if let presentedtoastView = window?.viewWithTag(Tag) {
                presentedtoastView.removeFromSuperview()
            }
            self.tag = Tag
            window?.addSubview(self)
            
            self.label.translatesAutoresizingMaskIntoConstraints = false
            self.translatesAutoresizingMaskIntoConstraints = false
            
            self.anchor(top: nil, leading: window?.leadingAnchor ?? NSLayoutXAxisAnchor(), bottom: window?.bottomAnchor ?? NSLayoutYAxisAnchor(), trailing: window?.trailingAnchor ?? NSLayoutXAxisAnchor(), padding:UIEdgeInsets(top: 0, left: leftRightPadding, bottom: BottomPadding, right: leftRightPadding))
            
            self.label.anchor(top: self.topAnchor, leading: self.leadingAnchor, bottom: self.bottomAnchor, trailing: self.trailingAnchor, padding:UIEdgeInsets(top: textPadding, left: textPadding, bottom: textPadding, right: textPadding))
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func present(completion: @escaping () -> () = {}) {
        self.alpha = 1
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(duration * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
            self.removeFromSuperview()
            completion()
        })
    }
}

