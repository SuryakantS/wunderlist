//
//  FleetAnnotationView.swift
//  WunderFleet
//
//  Created by Suryakant Sharma on 18/08/19.
//  Copyright © 2019 Suryakant Sharma. All rights reserved.
//

import Foundation
import MapKit

class FleetAnnotationView: MKAnnotationView {
    override var annotation: MKAnnotation? {
        willSet {
            guard let _ = newValue as? FleetViewModel else { return }
            canShowCallout = true
            calloutOffset = CGPoint(x: -5, y: 5)
            image = #imageLiteral(resourceName: "fleet")
        }
    }
    func moreButtonClicked(){
        print("morebuttonClicked")
    }
}
