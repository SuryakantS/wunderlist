//
//  NetworkManager.swift
//  WunderFleet
//
//  Created by Suryakant Sharma on 15/08/19.
//  Copyright © 2019 Suryakant Sharma. All rights reserved.
//

import Foundation

class NetworkManager {
    typealias completionHandler = (_ data: Data?, _ error: NetworkError?) -> Void
    
    private let session : URLSessionProtocol
    init(session: URLSessionProtocol = URLSession.shared) {
        self.session = session
    }
    
    func get(request: URLRequest, completionHandler: @escaping completionHandler) {
        //check network reachablity and return if device is not connected with network.
        if(!ReachabilityManager.sharedInstance.isNetworkWorking){
            completionHandler(nil,.noNetwork)
            return;
        }
        let task = self.session.dataTask(with: request) { (data, response, error) in
            guard let httpResponse = response as? HTTPURLResponse, let receivedData = data
                else {
                    completionHandler(nil,.noData)
                    return
            }
            if error == nil && self.isSuccessCode(httpResponse){
                completionHandler(receivedData, .noError)
            }
            else {
                completionHandler(nil, .someError)
            }
        }
        task.resume()
    }
    private func isSuccessCode(_ statusCode: Int) -> Bool {
        return statusCode == NetworkErrorCode.httpOk //(200 ..< 300) ~= response.statusCode
    }
    private func isSuccessCode(_ response: URLResponse?) -> Bool {
        guard let urlResponse = response as? HTTPURLResponse else {
            return false
        }
        return isSuccessCode(urlResponse.statusCode)
    }
}
