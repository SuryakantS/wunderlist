//
//  RequestCreator.swift
//  WunderFleet
//
//  Created by Suryakant Sharma on 15/08/19.
//  Copyright © 2019 Suryakant Sharma. All rights reserved.
//

import Foundation

class RequestCreator {
    
    class func createRequest(type:RequestType, fleetId: Int? = nil) -> URLRequest? {
        var urlStr : String?
        switch type {
        case .FleetList:
            urlStr = URLConstant.baseURL + URLConstant.fleetList
        case .FleetDetail:
            if let id  = fleetId{
                urlStr = URLConstant.baseURL + URLConstant.fleetDetailPath + String(id)
            }
        case .FleetBook:
            urlStr = URLConstant.bookFleet
        }
        guard let str = urlStr else { return nil }
        guard let url = URL(string: str) else { return nil }
        var request = URLRequest(url: url)
        if type == .FleetBook, let id = fleetId {
            request.httpMethod = HttpsMethods.POST.rawValue
            request.addValue("Bearer df7c313b47b7ef87c64c0f5f5cebd6086bbb0fa", forHTTPHeaderField: "Authorization")
            let parameters: [String: Int] = [NetworkKey.carIdKey : id]
            do {
                let data = try JSONSerialization.data(withJSONObject: parameters)
                request.httpBody = data
            } catch {
                return nil
            }
        }
        return request
    }
}
