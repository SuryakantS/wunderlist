//
//  UIColorExtension.swift
//  
//
//  Created by Suryakant Sharma on 15/08/19.
//  Copyright © 2019 Suryakant Sharma. All rights reserved.
//

import UIKit

extension UIColor {
    static let appThemeColor = UIColor.black
}
