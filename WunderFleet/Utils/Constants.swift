//
//  Constants.swift
//  
//
//  Created by Suryakant Sharma on 07/08/19.
//  Copyright © 2019 Suryakant Sharma. All rights reserved.
//

import Foundation
import UIKit


//MARK:- Enum declaration
public enum NetworkError {
    case noData
    case noNetwork
    case someError
    case invalidSyntax
    case noError
    case invalidData
}
public enum HttpsMethods: String{
    case GET
    case POST
}

public enum RequestType {
    case FleetList
    case FleetDetail
    case FleetBook
}


public struct FleetDetailViewConstants{
    static let imagePadding: UIEdgeInsets = UIEdgeInsets(top: 60, left: 10, bottom: 10, right: 10)
    static let padding: UIEdgeInsets = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
    static let size : CGSize = CGSize(width: 300, height: 300)
    static let ButtonSize : CGSize = CGSize(width: 100, height: 20)
    static let corderRadius: CGFloat = 5.0
}

public struct ReachabilityConstant {
    static let kNetworkReachability = -100
    static let kErrorDomain = "com.WunderFleet"
    static let kReachabilityChange = "ReachabilityChangedNotification"
}

public struct NetworkErrorCode {
    static let httpOk = 200
}

public struct URLConstant  {
    static let baseURL = "https://s3.eu-central-1.amazonaws.com/wunderfleet-recruiting-dev/"
    static let fleetList = "cars.json"
    static let fleetDetailPath = "cars/"
    static let bookFleet = "https://4i96gtjfia.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-mobile-dev-quick-rental"
}

public struct NetworkKey {
    static let carIdKey = "carId"
}

public struct MapViewConstants {
    static let regionRadius: Double = 1000
}
