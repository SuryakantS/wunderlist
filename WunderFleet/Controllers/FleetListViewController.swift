//
//  FleetListViewController.swift
//  WunderFleet
//
//  Created by Suryakant Sharma on 16/08/19.
//  Copyright © 2019 Suryakant Sharma. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class FleetListViewController: UIViewController {
    
    fileprivate var fleetList: [FleetViewModel]?
    private var fleetViewModelHandler: FleetViewModelHandler!
    private var networkManager: NetworkManager!
    var spinner : UIActivityIndicatorView?
    var selectedAnnotation: MKAnnotation?

    @IBOutlet weak var mapView: MKMapView!
    lazy var locationManager: CLLocationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapViewSetup()
        requestLocationAccess()
        self.title = "fleet.name.title".localize()
        updateView()
        if let location = locationManager.location {
            centerMapOnLocation(location: location)
        }
    }
    
    private func mapViewSetup(){
        mapView.showsUserLocation = true;
        mapView.delegate = self
        mapView.mapType = .standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        locationManager.delegate = self
    }

    private func updateView() {
        ActivityIndicator.shared.start(to: view)
        networkManager = NetworkManager()
        self.fleetViewModelHandler = FleetViewModelHandler(networkManager: networkManager, requestType: .FleetList, errorHandler: {  [unowned self] error in
            //handle all the error here
            ActivityIndicator.shared.stop()
            handleError(error: error, for: self);
        })
        // setting up the bindings
        self.fleetViewModelHandler.bindToSourceViewModels = { [unowned self] (fleetList: [FleetViewModel]) in
            //handle all the error here
            ActivityIndicator.shared.stop()
            self.fleetList = fleetList;
            self.mapView.addAnnotations(fleetList)
        }
        self.fleetViewModelHandler.errorToSourceViewModels = {  [unowned self] (error: NetworkError?) in
            //handle all the error here
            ActivityIndicator.shared.stop()
            handleError(error: error, for: self);
        }
    }
}

//MARK:- MapView methods
extension FleetListViewController {

    func requestLocationAccess() {
        let status = CLLocationManager.authorizationStatus()
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            break
        case .denied, .restricted:
            break
        default:
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, MapViewConstants.regionRadius, MapViewConstants.regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    //MARK:- MapView helper methods
    func toggleAnnotation(flag: Bool){
        for annotation in mapView.annotations {
            if let selectedAnno = self.selectedAnnotation, annotation !== selectedAnno {
                mapView.view(for: annotation)?.isHidden = flag;
            }
        }
    }
}

