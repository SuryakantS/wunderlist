
//
//  FleetDetailViewController.swift
//  WunderFleet
//
//  Created by Suryakant Sharma on 19/08/19.
//  Copyright © 2019 Suryakant Sharma. All rights reserved.
//

import UIKit

class FleetDetailViewController : UIViewController {
    
    private var fleetViewModel: FleetDetailModel
    
    private let vehicleImageView : CachedImageView = {
        let iv = CachedImageView()
        iv.contentMode = UIViewContentMode.scaleAspectFit
        iv.layer.cornerRadius = 5.0
        iv.translatesAutoresizingMaskIntoConstraints = false;
        return iv
    }()

    private let titleLbl: UILabel = {
        let label = UILabel();
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0;
        label.text = "Manfred"
        label.lineBreakMode = .byWordWrapping;
        label.textColor = .white
        return label;
    }();
    
    private let pricingTime : UILabel = {
        let label = UILabel();
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 13.0);
        label.textColor = .white
        label.text = "0,22/km"
        return label;
    }();
    
    private let bookBtn: UIButton = {
        let button:UIButton = UIButton(type: .custom)
        button.backgroundColor = .white
        button.setTitleColor(.black, for: .normal)
        button.setTitle("Book Ride", for: .normal)
        button.addTarget(self, action:#selector(bookTheRide), for: .touchUpInside)
        return button
    }();
    
    private let damagedDiscription : UILabel = {
        let label = UILabel();
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0;
        label.textColor = .white
        label.lineBreakMode = .byWordWrapping;
        label.text = "The Dark Knight of Gotham City begins his war on crime with his first major enemy being the clownishly homicidal Joker, who has seized control of Gotham's underworld."
        label.translatesAutoresizingMaskIntoConstraints = false
        return label;
    }();
    
    init(fleetViewModel: FleetDetailModel) {
        self.fleetViewModel = fleetViewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        self.title = "fleet.list.header.title".localize()
        setupValues()
    }
    private func setupValues(){
        self.titleLbl.text = fleetViewModel.title
        self.pricingTime.text = fleetViewModel.pricingTime
        if fleetViewModel.damageDescription?.isEmpty == false {
            self.damagedDiscription.text = fleetViewModel.damageDescription
        }
        else{
            self.damagedDiscription.text = ""
        }
        if let url = fleetViewModel.vehicleTypeImageUrl {
            vehicleImageView.loadImage(placeHolder: #imageLiteral(resourceName: "imagePlaceHolder"), urlString: url, completion: nil)
        }
    }
    
    private func setupView(){
        self.view.backgroundColor = .black
        vehicleImageViewSetup()
        setupDetailLabels()
        addButton()
    }
    func vehicleImageViewSetup(){
        view.addSubview(vehicleImageView)
        NSLayoutConstraint.activate([ self.vehicleImageView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor)])
        vehicleImageView.anchor(top: self.view.topAnchor, leading: nil, bottom: nil, trailing: nil, padding: FleetDetailViewConstants.imagePadding, size: FleetDetailViewConstants.size)
    }
    func setupDetailLabels(){
        view.addSubview(titleLbl)
        titleLbl.anchor(top: vehicleImageView.bottomAnchor, leading: self.view.leadingAnchor, bottom: nil, trailing: self.view.trailingAnchor, padding: FleetDetailViewConstants.padding)
        
        view.addSubview(pricingTime)
        pricingTime.anchor(top: titleLbl.bottomAnchor, leading: self.view.leadingAnchor, bottom: nil, trailing: self.view.trailingAnchor, padding: FleetDetailViewConstants.padding)
        
        view.addSubview(damagedDiscription)
        damagedDiscription.anchor(top: pricingTime.bottomAnchor, leading: self.view.leadingAnchor, bottom: nil, trailing: self.view.trailingAnchor, padding: FleetDetailViewConstants.padding)
    }
    
    func addButton(){
        view.addSubview(bookBtn)
        bookBtn.anchor(top: nil, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor, padding: FleetDetailViewConstants.padding)
    }
    
    func bookTheRide(){
        guard let request = RequestCreator.createRequest(type: .FleetBook, fleetId: self.fleetViewModel.carId) else { return }
        let networkManager = NetworkManager()
        networkManager.get(request: request) { (data, error) in
            if error == .noError {
                DispatchQueue.main.async {
                    let toast = ToastView(message: "fleet.book.message".localize(), trucateMiddle: false, numberOfLines: 1)
                    toast.present(completion: {})
                    bookBtn.isEnabled = false;
                }
            }
            else{
                DispatchQueue.main.async {
                    let toast = ToastView(message: "image.operation.error".localize(), trucateMiddle: false, numberOfLines: 1)
                    toast.present(completion: {})
                }
            }
        }
    }
}
