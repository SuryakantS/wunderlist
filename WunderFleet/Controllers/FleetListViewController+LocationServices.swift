//
//  FleetListViewController+LocationServices.swift
//  WunderFleet
//
//  Created by Suryakant Sharma on 17/08/19.
//  Copyright © 2019 Suryakant Sharma. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

extension FleetListViewController : CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        mapView.mapType = MKMapType.standard
        let span = MKCoordinateSpanMake(0.5, 0.5)
        let region = MKCoordinateRegion(center: locValue, span: span)
        mapView.setRegion(region, animated: true)
    }
}

//MARK: Mapkit delegates
extension FleetListViewController : MKMapViewDelegate{
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        self.selectedAnnotation = view.annotation
        toggleAnnotation(flag: true)
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(calloutTapped(sender:)))
        view.addGestureRecognizer(gesture)

    }
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        toggleAnnotation(flag: false)
        self.selectedAnnotation = nil
    }
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }
            
        else {
            let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "annotationView") ?? FleetAnnotationView(annotation: annotation, reuseIdentifier: "annotationView")
            return annotationView
        }
    }
        
    func calloutTapped(sender:UITapGestureRecognizer) {
        guard let annotation = (sender.view as? MKAnnotationView)?.annotation as? FleetViewModel else { return }
        let detailCoordinator = FleetDetailViewCoordinator.shared
        detailCoordinator.fleetViewModel = annotation;
        detailCoordinator.navController = self.navigationController
        detailCoordinator.start()
    }

}
