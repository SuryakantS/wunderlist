//
//  JSONParser.swift
//  WunderFleet
//
//  Created by Suryakant Sharma on 10/08/19.
//  Copyright © 2019 Suryakant Sharma. All rights reserved.
//

import UIKit

/***
 This class is resposible to recode all the JSON coming as response-
 to a decodable objects.
 **/
class JSONParser {
    class func decodeJson<T:Decodable>(from data: Data) -> [T]? {
        let decoder = JSONDecoder()
        do {
            let list = try decoder.decode([T].self, from: data)
            return list
        }
        catch let error {
            print(error)
            return nil
        }
    }
}
