//
//  Coordinators.swift
//  WunderFleet
//
//  Created by Suryakant Sharma on 17/08/19.
//  Copyright © 2019 Suryakant Sharma. All rights reserved.
//

import UIKit

protocol Coordinator: class {
    func start()
}
