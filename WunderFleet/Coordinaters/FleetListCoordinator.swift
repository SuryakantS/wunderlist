//
//  FleetListCoordinator.swift
//  WunderFleet
//
//  Created by Suryakant Sharma on 17/08/19.
//  Copyright © 2019 Suryakant Sharma. All rights reserved.
//

import UIKit

class FleetListCoordinator: Coordinator {
    
    static let shared = FleetListCoordinator()
    var navController: UINavigationController?
    
    //private Initializer
    private init(){}
    
    func start(){
        if let rootVC = setupRootViewCopntroller(), let window = UIApplication.shared.delegate?.window {
            window?.rootViewController = rootVC
        }
    }
    
    //setup of rootViewController here.
    func setupRootViewCopntroller() -> UINavigationController? {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let fleetViewController = storyboard.instantiateViewController(withIdentifier: "FleetListViewController")
        navController = UINavigationController(rootViewController: fleetViewController)
        UINavigationBar.appearance().barTintColor = .appThemeColor
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]

//        UINavigatioeenBar.appearance().titleTextAttributes = [NSAttributedString : UIColor.white]
        return navController
    }
}

