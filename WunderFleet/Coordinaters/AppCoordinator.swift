//
//  AppCoordinator.swift
//  WunderFleet
//
//  Created by Suryakant Sharma on 17/08/19.
//  Copyright © 2019 Suryakant Sharma. All rights reserved.
//

import Foundation
import UIKit

class AppCoordinator: Coordinator {
    func start(){
        let fleetListCoordinator = FleetListCoordinator.shared
        fleetListCoordinator.start()
    }
}
