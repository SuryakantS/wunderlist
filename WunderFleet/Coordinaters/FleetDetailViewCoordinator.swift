//
//  FleetDetailViewCoordinator.swift
//  WunderFleet
//
//  Created by Suryakant Sharma on 19/08/19.
//  Copyright © 2019 Suryakant Sharma. All rights reserved.
//

import UIKit

class FleetDetailViewCoordinator: Coordinator {
    
    static let shared = FleetDetailViewCoordinator()
    var navController: UINavigationController?
    var fleetViewModel: FleetViewModel?
    var fleetViewModelHandler : FleetViewModelHandler?
    //private Initializer
    private init(){}
    
    func start(){
        if let model = fleetViewModel {
            let networkManager = NetworkManager()
            fleetViewModelHandler = FleetViewModelHandler(networkManager: networkManager, requestType:.FleetDetail,fleetId:model.carId, errorHandler: { error in
                if let topViewController = self.navController?.topViewController{
                    handleError(error: error, for: topViewController);
                }
            })
            if let handler = fleetViewModelHandler {
                // setting up the bindings
                handler.bindToSourceViewDetailModel = { [unowned self] (fleetDetails: FleetDetailModel) in
                    DispatchQueue.main.async {
                        let favViewController = FleetDetailViewController(fleetViewModel: fleetDetails)
                        self.navController?.pushViewController(favViewController, animated: true)
                    }
                }
                handler.errorToSourceViewModels = {  [unowned self] (error: NetworkError?) in
                    if let topViewController = self.navController?.topViewController, let err = error {
                        DispatchQueue.main.async {
                            handleError(error: err, for: topViewController);
                        }
                    }
                }
            }
        }
    }
}
