//
//  FleetViewModelHandler.swift
//  WunderFleet
//
//  Created by Suryakant Sharma on 17/08/19.
//  Copyright © 2019 Suryakant Sharma. All rights reserved.
//

import Foundation

@objcMembers class FleetViewModelHandler : NSObject {
    typealias CompletionHandler = (_ viewModel: FleetViewModel?, _ index:Int? , _ error : NetworkError) -> Void
    typealias ErrorHandler = ((_ error :NetworkError?) -> ())
    private(set) var fleetViewModels :[FleetViewModel] = [FleetViewModel]() {
        didSet {
            self.bindToSourceViewModels(fleetViewModels)
        }
    }
    
    private(set) var fleetDetails :FleetDetailModel? = nil{
        didSet {
            if let fleet = fleetDetails{
                self.bindToSourceViewDetailModel(fleet)
            }
        }
    }
    var bindToSourceViewDetailModel :((_ fleetDetail :FleetDetailModel) -> ()) = {_ in   }
    var bindToSourceViewModels :((_ fleetViewModels :[FleetViewModel]) -> ()) = {_ in   }
    var errorToSourceViewModels: ErrorHandler = { error in  }
    private var networkManager: NetworkManager
    
    init(networkManager: NetworkManager, requestType: RequestType, fleetId: Int? = nil , errorHandler: @escaping ErrorHandler) {
        self.networkManager = networkManager
        super.init()
        errorToSourceViewModels = errorHandler
        // call populate sources
        populateFleetList(type: requestType,fleetId: fleetId)
    }
    
    func populateFleetList(type: RequestType, fleetId: Int? = nil ) {
        guard let request = RequestCreator.createRequest(type: type, fleetId: fleetId) else {
            self.errorToSourceViewModels(.invalidSyntax)
            return
        }
        self.networkManager.get(request: request) { [weak self] (data, error) in
            if error == .noError, let responseData = data {
                if type == .FleetList{
                    if let fleetList: [FleetModel]  = JSONParser.decodeJson(from: responseData){
                        var list = [FleetViewModel]()
                        fleetList.forEach { (aFleet) in
                            let fleet = FleetViewModel(fleet: aFleet)
                            list.append(fleet);
                        }
                        self?.fleetViewModels += list
                        list.removeAll()
                    }
                }
                else if type == .FleetDetail{
                    let decoder = JSONDecoder()
                    do {
                        let detail = try decoder.decode(FleetDetailModel.self, from: responseData)
                        self?.fleetDetails = detail
                    }
                    catch {
                        return
                    }
                }

            }
            else {
                self?.errorToSourceViewModels(error)
            }
        }
    }
    
    func add(viewModel: FleetViewModel) {
        self.fleetViewModels.append(viewModel)
    }
    
    func fleet(at index:Int) -> FleetViewModel {
        return self.fleetViewModels[index]
    }
    func index(of viewModel:FleetViewModel) -> Int? {
        return self.fleetViewModels.index{ $0 === viewModel }//firstIndex{ $0 === viewModel }
    }
    func cleanViewModelList(){
        self.fleetViewModels.removeAll();
    }
    
    func update( viewModel:FleetViewModel, onUpdate: @escaping CompletionHandler){
        
    }
}
