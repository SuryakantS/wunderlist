//
//  FleetDetailViewModel.swift
//  WunderFleet
//
//  Created by Suryakant Sharma on 19/08/19.
//  Copyright © 2019 Suryakant Sharma. All rights reserved.
//

import CoreLocation

struct FleetDetailViewModel {
    let title : String?
    let vehicleTypeId : Int?
    let imageUrl: String?
    let isDamaged : Bool?
    let damageDescription: String?
    let pricingTime: String?
    let coordinate: CLLocationCoordinate2D?
}
