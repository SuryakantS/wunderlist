//
//  FleetViewModel.swift
//  WunderFleet
//
//  Created by Suryakant Sharma on 17/08/19.
//  Copyright © 2019 Suryakant Sharma. All rights reserved.
//

import Foundation
import MapKit

class FleetViewModel: NSObject, MKAnnotation {
    let carId: Int?
    let title: String?
    let licencePlate : String?
    let coordinate: CLLocationCoordinate2D
    
    init(fleet: FleetModel) {
        self.carId = fleet.carId
        if true != fleet.title?.isEmpty {
            self.title = fleet.title
        }
        else {
            self.title = "fleet.name.title".localize()
        }
        self.licencePlate = fleet.licencePlate
        if let lat = fleet.lat, let lon = fleet.lon {
            self.coordinate = CLLocationCoordinate2D(latitude: lat, longitude:lon)
        }
        else {
            self.coordinate = CLLocationCoordinate2D()
        }
        super.init()
    }
}
