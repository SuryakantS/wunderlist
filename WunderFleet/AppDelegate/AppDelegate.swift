//
//  AppDelegate.swift
//  WunderFleet
//
//  Created by Suryakant Sharma on 14/08/19.
//  Copyright © 2019 Suryakant Sharma. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        //start nerwork reachabiliy test
        ReachabilityManager.sharedInstance.allocRechability();
        UIApplication.shared.statusBarStyle = .lightContent
        
        //setup window
        window = UIWindow(frame: UIScreen.main.bounds)
        //App coordinator setup and present initial view
        let appCoordinator = AppCoordinator()
        appCoordinator.start()
        window?.makeKeyAndVisible()
        return true
    }
}

