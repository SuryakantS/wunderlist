//
//  RequestCreatorTests.swift
//  WunderFleetTest
//
//  Created by Suryakant Sharma on 15/08/19.
//  Copyright © 2019 Suryakant Sharma. All rights reserved.
//

import XCTest
@testable import WunderFleet

class RequestCreatorTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testCreateURLForFleetList(){
        let request = RequestCreator.createRequest(type: .FleetList)
        XCTAssert("https://s3.eu-central-1.amazonaws.com/wunderfleet-recruiting-dev/cars.json" == request?.url?.absoluteString)
    }
    
    func testCreateURLForFleetDetail(){
        let request = RequestCreator.createRequest(type: .FleetDetail, fleetId: 15)
        XCTAssert("https://s3.eu-central-1.amazonaws.com/wunderfleet-recruiting-dev/cars/15" == request?.url?.absoluteString)
    }

    func testCreateURLForFleetBook(){
        let request = RequestCreator.createRequest(type: .FleetBook, fleetId: 15)
        XCTAssert("https://4i96gtjfia.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-mobile-dev-quick-rental" == request?.url?.absoluteString)
        XCTAssert(request?.httpMethod == HttpsMethods.POST.rawValue)
    }
}
