//
//  NetworkManagerTests.swift
//  WunderFleetTest
//
//  Created by Suryakant Sharma on 15/08/19.
//  Copyright © 2019 Suryakant Sharma. All rights reserved.
//

import XCTest
@testable import WunderFleet

class NetworkManagerTests: XCTestCase {
    
    var networkManager: NetworkManager!
    
    let session = MockURLSession()
    override func setUp() {
        super.setUp()
        networkManager = NetworkManager(session: session)
    }
    override func tearDown() {
        networkManager = nil
        super.tearDown()
    }

    func testGetRequestWithURL(){
        guard let url = URL(string: "https://mockurl.com") else {
            fatalError("URL can't be empty")
        }
        let urlRequest = URLRequest(url: url)
        networkManager.get(request: urlRequest) { (data, error) in
            //data and error return
        }
        XCTAssert(session.lastURL == url)
    }
    func testGetResumeCalled() {
        let dataTask = MockURLSessionDataTask()
        session.nextDataTask = dataTask
        guard let url = URL(string: "https://mockurl") else {
            fatalError("URL can't be empty")
        }
        let urlRequest = URLRequest(url: url)
        networkManager.get(request: urlRequest) { (data, error) in
            //data and error return
        }
        
        XCTAssert(dataTask.resumeWasCalled)
    }

    func testGetShouldReturnData() {
        let expectedData = "{}".data(using: .utf8)
        
        session.nextData = expectedData
        guard let url = URL(string: "https://mockurl") else {
            fatalError("URL can't be empty")
        }
        var actualData: Data?
        let urlRequest = URLRequest(url: url)
        networkManager.get(request: urlRequest) { (data, error) in
            actualData = data
        }
        XCTAssertNotNil(actualData)
    }
}
