//
//  JSONParserTests.swift
//  WunderFleetTests
//
//  Created by Suryakant Sharma on 15/08/19.
//  Copyright © 2019 Suryakant Sharma. All rights reserved.
//

import XCTest
@testable import WunderFleet

class JSONParserTests: XCTestCase {
    
    let validJson = """
        [
    {
        "carId": 1,
        "title": "Manfred",
        "lat": 51.5156,
        "lon": 7.4647,
        "licencePlate": "FBL 081",
        "fuelLevel": 27,
        "vehicleStateId": 0,
        "vehicleTypeId": 1,
        "pricingTime": "0,22/km",
        "pricingParking": "0,05/min",
        "reservationState": 0,
        "isClean": false,
        "isDamaged": true,
        "distance": "",
        "address": "Bissenkamp 4",
        "zipCode": "44135",
        "city": "Dortmund",
        "locationId": 2
    },
    {
        "carId": 2,
        "title": "Anton",
        "lat": 51.511998333333,
        "lon": 7.4625316666667,
        "licencePlate": "162 NBT",
        "fuelLevel": 46,
        "vehicleStateId": 0,
        "vehicleTypeId": 2,
        "pricingTime": "",
        "pricingParking": "",
        "reservationState": 0,
        "isClean": true,
        "isDamaged": false,
        "distance": "",
        "address": "Kuhstraße 13",
        "zipCode": "44137",
        "city": "Dortmund",
        "locationId": 2
    }]
""".data(using: .utf8)
    
    let invalidJson = """
    {
        "carId": 1,
        "title": "Manfred",
        "lat": 51.5156,
        "lon": 7.4647,
        "licencePlate": "FBL 081",
        "fuelLevel": 27,
        "vehicleStateId": 0,
        "vehicleTypeId": 1,
        "pricingTime": "0,22/km",
        "pricingParking": "0,05/min",
        "reservationState": 0,
        "isClean": false,
        "isDamaged": true,
        "distance": "",
        "address": "Bissenkamp 4",
        "zipCode": "44135",
        "city": "Dortmund",
        "locationId": 2
    },
    {
        "carId": 2,
        "title": "Anton",
        "lat": 51.511998333333,
        "lon": 7.4625316666667,
        "licencePlate": "162 NBT",
        "fuelLevel": 46,
        "vehicleStateId": 0,
        "vehicleTypeId": 2,
        "pricingTime": "",
        "pricingParking": "",
        "reservationState": 0,
        "isClean": true,
        "isDamaged": false,
        "distance": "",
        "address": "Kuhstraße 13",
        "zipCode": "44137",
        "city": "Dortmund",
        "locationId": 2
    }
""".data(using: .utf8)
    
    
    
    let fleetDetailJson = """
{
"carId": 1,
"title": "Manfred",
"lat": 51.5156,
"lon": 7.4647,
"licencePlate": "FBL 081",
"fuelLevel": 27,
"vehicleStateId": 0,
"vehicleTypeId": 1,
"pricingTime": "0,22/km",
"pricingParking": "0,05/min",
"reservationState": 0,
"isClean": false,
"isDamaged": true,
"distance": "",
"address": "Bissenkamp 4",
"zipCode": "44135",
"city": "Dortmund",
"locationId": 2
}
"""
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testJSONParserWithValidJSON(){
        let fleetList : [FleetModel] = JSONParser.decodeJson(from: validJson!)!
        XCTAssert(2 == fleetList.count)
        XCTAssert(1 == fleetList.first?.carId)
        XCTAssert("FBL 081" == fleetList.first?.licencePlate)
        XCTAssert(51.5156 == fleetList.first?.lat)
        XCTAssert(7.4647 == fleetList.first?.lon)
        XCTAssert("Manfred" == fleetList.first?.title)

        XCTAssert(2 == fleetList.last?.carId)
        XCTAssert("162 NBT" == fleetList.last?.licencePlate)
        XCTAssert(51.511998333333 == fleetList.last?.lat)
        XCTAssert(7.4625316666667 == fleetList.last?.lon)
        XCTAssert("Anton" == fleetList.last?.title)

    }
    
    func testJSONParserWithInValidJSON(){
        let kittyList : [FleetModel]? = JSONParser.decodeJson(from: invalidJson!)
        XCTAssert(kittyList == nil)
    }
}
